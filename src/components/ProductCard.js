
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
	
	const { productName, description, price, _id } = productProp;
	console.log(productProp)

	return (

	<Container>
		<Row className="mt-3 mb-3">
			<Col xs= {12} md={4}> 	
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{productName}</Card.Title>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					<Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>	
				</Card.Body>
			</Card>
			</Col>
		</Row>
	</Container>

    );
}

/*

	<Link className="btn btn-primary" to="courseView">Details</Link>

*/
