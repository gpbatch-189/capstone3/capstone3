// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import Container from 'react-bootstrap/Container';
// import {useState} from 'react';

import {Fragment, useContext} from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Logo from './images/logo.png';


export default function AppNavbar() {

  const { user } = useContext(UserContext)

  //State to store the user information stored in the login page, 
  //const [user, setUser] = useState(localStorage.getItem("email"));

  /*

  email: admin@mail.com
    Syntax:
      localStorage.getItem("propertyName")
    getItem() method that returns value of a specified object item

  */

  console.log(user);

	return (
		<Navbar bg="light" variant="light" expand="lg" sticky="top">
      <Container>      
        {/* <Navbar.Brand as={ Link } to="/">The Bailiwick Academy Shop</Navbar.Brand> */}
        <Navbar.Brand href="/">
            <img
              alt=""
              src={Logo}
              width="100"
              height="40"
              className="d-inline-block align-top"
            />{' '}          
          </Navbar.Brand>

          
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="col-6 col-md-4">
            <Nav.Link as={ Link } to="/">Home</Nav.Link>
            <Nav.Link as={ Link } to="/products">Products</Nav.Link>
            {
                // if walang laman ng localStorage
            (user.id !== null) ?
                <>
                <Nav.Link as={ Link } to="/users/myorders">My Orders</Nav.Link>
                <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
              
                </>


                : //if may laman
                <Fragment>
                    <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                    <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                </Fragment>
            }
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>




		)
}