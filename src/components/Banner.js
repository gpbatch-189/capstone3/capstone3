import {Card, Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import BannerPic from './images/bannerIngredients.jpg';

export default function Banner () {


	return(
		<Container>
			{/* <Row>
				<Col className="p-5">
					<Card.Img src={Apf} className="img-fluid" alt="Card image" />
					<h1>Looking for Baking and Cooking Ingredients? </h1>
					<p>We ship from the Philippines -> WORLDWIDE</p>
					<Button variant="primary">SHOP NOW! </Button>
				</Col>
			</Row>  */}

			<Card className= "text-dark">
			<Card.Img src={BannerPic} className="img-fluid" alt="Card image" />
			<Card.ImgOverlay>
				<Card.Title>Looking for Baking and Cooking Ingredients?</Card.Title>
				<Card.Text>
					<p1>We ship from the Philippines -> WORLDWIDE</p1>
				</Card.Text>
		
				<Button variant="warning" as={Link} to={`/products`}>SHOP NOW</Button>	
			</Card.ImgOverlay>
			</Card>

			{/* <div class="hero-image">
			<div class="hero-text">
				<h1>I am John Doe</h1>
				<p>And I'm a Photographer</p>
				<button>Hire me</button>
			</div>
			</div> */}



		</Container>

		)
}

