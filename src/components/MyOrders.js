
import {Row, Col, Card, Container} from 'react-bootstrap';

export default function MyOrders({orderProp}) {
	
	const { productName, price } = orderProp;

	console.log(orderProp)

	return (

	<Container>
		<Row className="mt-3 mb-3">
			<Col xs= {12} md={4}> 	
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{productName}</Card.Title>				
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
			
				</Card.Body>
			</Card>
			</Col>
		</Row>
	</Container>

    );
}

/*

	<Link className="btn btn-primary" to="courseView">Details</Link>

*/
