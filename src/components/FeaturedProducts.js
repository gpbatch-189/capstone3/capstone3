import {Row, Col, Card, Button, Container} from 'react-bootstrap';



import Apf from './images/apf.png';
import Magimix from './images/MagimixYellow.png';
import Saf125 from './images/safyeast125g.png';

export default function FeaturedProducts() {

	return (

		<Container>
			<h2 style={{textAlign: "center"}}> Featured Products </h2>

			<Row className="mt-3 mb-3">
				<Col xs= {12} md={4}> 
					<Card className="cardHighlight p-3" >
					<Card.Img src={Apf} className="img-fluid" alt="Card image" />
				      <Card.Body>
				        <Card.Title>Wooden Spoon All Purpose Flour 1Kg</Card.Title>
				        <Card.Text>
				          Suitable for all types of baked goods such as bread, biscuits, pizza, cookies, muffins, etc. It is also used in thickening gravies and sauces.
				        </Card.Text>
				 		<Button variant="warning">Details</Button>
						 
				      </Card.Body>
				    </Card>
				</Col>


				<Col xs= {12} md={4}> 
					<Card className="cardHighlight p-3" >
					<Card.Img src={Magimix} className="img-fluid" alt="Card image" />
				      <Card.Body>
				        <Card.Title>Magimix Bread Improver Yellow (500g)</Card.Title>
				        <Card.Text>
						Produces a soft elastic crumb in all types of baked products. Provides longer lasting freshness with anti-staling properties. 
				        </Card.Text>
				 		<Button variant="warning">Details</Button>
				      </Card.Body>
				    </Card>

				</Col>


				<Col xs= {12} md={4}> 
					<Card className="cardHighlight p-3" >
					<Card.Img src={Saf125} className="img-fluid" alt="Card image" />
				      <Card.Body>
				        <Card.Title>SAF Gold Instant Yeast (125g)</Card.Title>
				        <Card.Text>
						Create your own breads with the use of this instant yeast.
						There's no need to bloom this yeast.

				        </Card.Text>
				 		<Button variant="warning">Details</Button>
				      </Card.Body>
				    </Card>

				</Col>

				

			</Row>

			

		</Container>	
		)

}