
// import{Fragment, useEffect, useState} from 'react';
// import ProductCard from '../components/ProductCard';

// export default function Products() {

//     const [products, setProducts] = useState([])

//     useEffect(() => {

//         fetch('http://localhost:4000/products/')
//         .then(res => res.json())
//         .then( data => {
//             console.log('from products page' + data)

//             setProducts(data.map(product => {

//                 return (
                
//                         <ProductCard key ={product._id} productProp={product} />
//                     )
//             }))

//         })


//     }, [])



//     return(
        
//         <Fragment> 
//            <h1 style={{textAlign: "center"}}> ALL PRODUCTS </h1>
//            {products}
//         </Fragment>    
     

//     )
// }


import { useState, useEffect, useContext } from 'react';
// import productData from './../data/products'
import UserView from  "../components/UserView"
import AdminView from  "../components/AdminView"
import UserContext from "../UserContext"

export default function Products() {

	const { user } = useContext(UserContext);

	const [productsData, setProductsData] = useState([])

	// console.log(productData)

	const fetchData = () => {
		fetch(`https://sheltered-ravine-76445.herokuapp.com/products`)
		.then(res => res.json())
		.then(data => {
			//.then has what is called a "self-contained scope"

			//Any code inside of this .then only exists inside of this .then, and therefore cannot be processed properly by React

			//to solve this problem, we use a state. By setting the new value of our state to be the data from our server, that state can be seen by our entire component
			setProductsData(data)
		})
	}

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		fetchData()
	}, [])

	return(user.isAdmin ?
		<AdminView productProp={productsData} fetchData={fetchData}/>
		:
		<UserView productProp={productsData}/>)
}
