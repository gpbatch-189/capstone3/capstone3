import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function ProductView() {

	const{ user } = useContext(UserContext)

	//Allow us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate(); //useHistory

	//The "useParams"
	const { productId } = useParams();

	const [productName, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const product = (productId) => {
		fetch(`https://sheltered-ravine-76445.herokuapp.com/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				price: price
			})

		})

		.then(res => res.json())
		.then(data => {
			
			console.log('.then after mag fetch' + data);

			if(data === true) {
				Swal.fire({
					title:"Congratulations!",
					icon: "success",
					text: "Nabili mo na to, NO cancel!"
				})

				navigate("/products")

			} else {
				Swal.fire({
					title:"Something went wrong",
					icon: "error",
					text: "Please try again!"
				})

			}
		})
	}

	useEffect(() => {
		console.log('from useEffect after' + productId)

		fetch(`https://sheltered-ravine-76445.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log('from useEffect after .then' + data)

			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)

		})


	}, [productId])

	return (
		
		<Container className="mt-3"> 
			<Row> 
				<Col lg={{span: 6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
		

							{
								user.id !== null ?
							
								

								<Button variant="primary" onClick={() => product(productId)}>Add To Cart</Button>
								
								
								:

								 <Link className="btn btn-danger" to="/login" > Login to Buy</Link>

							}
							
						</Card.Body>
					</Card>
				</Col>

			</Row>
		</Container>
		)
}