
import{Fragment, useEffect, useState} from 'react';
import MyOrders from '../components/MyOrders';




export default function OrderHistory() {


    const [orders, setOrders] = useState([])

    useEffect(() => {

        fetch(`https://sheltered-ravine-76445.herokuapp.com/users/myorders`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			}
		})


        .then(res => res.json())
        .then( data => {
            console.log(data.orders)

            setOrders(data.orders.map(order => {

                return (
                
                        <MyOrders key ={order._id} orderProp={order} />
                    )
            }))

        })


    }, [])



    return(
        
        <Fragment> 
           <h1 style={{textAlign: "center"}}> Order History </h1>
           {orders}
        </Fragment>    
     


    )
}